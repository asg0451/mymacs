;; -*- lexical-binding: t; -*-
(require 'my:core)
(require 'my:code)

(use-package js2-mode
  :defer t
  :init
  (progn
    (my:code::ggtags-def
      :keymaps 'js2-mode-map)
    (general-create-definer my:javascript::general-def
      :wrapping my:core::mmode-general-def
      :prefix-command #'my:javascript::comand
      :prefix-map 'my:javascript:map
      :keymaps '(js-mode-map js-jsx-mode-map)
      :major-modes t))
  :gfhook ('js-mode-hook
           #'js2-mode)
  :general
  (my:javascript::general-def
    "w" 'js2-mode-toggle-warnings-and-errors
    "z" '(:ignore t :wk "hideshow")
    "zc" 'js2-mode-hide-element
    "zo" 'js2-mode-show-element
    "zr" 'js2-mode-show-all
    "ze" 'js2-mode-toggle-element
    "zF" 'js2-mode-toggle-hide-functions
    "zC" 'js2-mode-toggle-hide-comments))
(use-package tern
  :after js2-mode
  :general
  (my:javascript::general-def
    "rrV" 'tern-rename-variable
    "h" '(:ignore t :wk "help")
    "hd" 'tern-get-docs
    "ht" 'tern-get-type
    "g" '(:ignore t :wk "goto")
    "gG" 'tern-find-definition-by-name
    "gg" 'tern-pop-find-definition))

(use-package js2-refactor
  :after js2-mode
  :general
  (my:javascript::general-def
    "r3i" #'js2r-ternary-to-if
    "rag" #'js2r-add-to-globals-annotation
    "rao" #'js2r-arguments-to-object
    "rba" #'js2r-forward-barf
    "rca" #'js2r-contract-array
    "rco" #'js2r-contract-object
    "rcu" #'js2r-contract-function
    "rea" #'js2r-expand-array
    "ref" #'js2r-extract-function
    "rem" #'js2r-extract-method
    "reo" #'js2r-expand-object
    "reu" #'js2r-expand-function
    "rev" #'js2r-extract-var
    "rig" #'js2r-inject-global-in-iife
    "rip" #'js2r-introduce-parameter
    "riv" #'js2r-inline-var
    "rlp" #'js2r-localize-parameter
    "rlt" #'js2r-log-this
    "rrv" #'js2r-rename-var
    "rsl" #'js2r-forward-slurp
    "rss" #'js2r-split-string
    "rsv" #'js2r-split-var-declaration
    "rtf" #'js2r-toggle-function-expression-and-declaration
    "ruw" #'js2r-unwrap
    "rvt" #'js2r-var-to-this
    "rwi" #'js2r-wrap-buffer-in-iife
    "rwl" #'js2r-wrap-in-for-loop
    "k" #'js2r-kill
    "xmj" #'js2r-move-line-down
    "xmk" #'js2r-move-line-up))
(use-package import-js
  :after js2-mode
  :general
  (my:javascript::general-def
    "i" '(:ignore t :wk "import")
    "ii" #'import-js-import
    "ig" #'import-js-goto))
(use-package prettier-js
  :after js2-mode
  :general
  (my:javascript::general-def
    "=" #'prettier-js))
(use-package add-node-modules-path
  :gfhook
  ('(css-mode-hook js2-mode-hook json-mode-hook)
   #'add-node-modules-path))

(use-package nodejs-repl
  :after js2-mode
  :general
  (my:javascript::general-def
    "n" '(:ignore t :wl "node")
    "n'" 'nodejs-repl
    "ns" 'nodejs-repl
    "ni" 'nodejs-repl-switch-to-repl
    "ne" 'nodejs-repl-send-last-expression
    "nb" 'nodejs-repl-send-buffer
    "nl" 'nodejs-repl-send-line
    "nr" 'nodejs-repl-send-region))
(use-package skewer-mode
  :after js2-mode
  :general
  (my:javascript::general-def
    "s" '(:ignore t :wk "skewer")
    "se" 'skewer-eval-last-expression
    "sE" 'skewer-eval-print-last-expression
    "sb" 'skewer-load-buffer
    "sf" 'skewer-eval-defun
    "ss" 'skewer-repl))

(provide 'my:javascript)

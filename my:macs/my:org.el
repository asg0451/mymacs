;; -*- lexical-binding: t; -*-
(require 'my:core)
(require 'my:code)

(use-package org
  :straight org-plus-contrib
  :preface (general-create-definer my:org::general-def
            :wrapping my:core::mmode-general-def
            :keymaps 'org-mode-map
            :major-modes t
            :package 'org
            :prefix-command 'my:org::command
            :prefix-map 'my:org:map)
  :init (progn
          (setq org-adapt-indentation nil
                org-return-follows-link t
                org-hide-emphasis-markers nil
                org-link-descriptive nil
                org-highlight-latex-and-related nil
                org-agenda-files "~/store/my_sync/org/agendarc"
                org-highlight-latex-and-related '(latex script entities)
                org-todo-keywords '("TODO" "|" "DONE" "CANCELED" "NODO"))
          (my:core::general-def
            "o" '(:ignore t :wk "org")
            "o#" #'org-agenda-list-stuck-projects
            "o/" #'org-occur-in-agenda-files
            "oa" #'org-agenda-list
            "oc" #'org-capture
            "oe" #'org-store-agenda-views
            "ol" #'org-store-link
            "om" #'org-tags-view
            "oo" #'org-agenda
            "op" #'org-projectile-capture-for-current-project
            "os" #'org-search-view
            "ot" #'org-todo-list

            "oC" '(nil :wk "clock")
            "oCc" #'org-clock-cancel
            "oCg" #'org-clock-goto
            "oCI" #'org-clock-in-last
            "oCi" #'org-clock-in
            "oCj" #'org-clock-jump-to-current-clock
            "oCo" #'org-clock-out
            "oCO" #'org-clock-out-if-current
            "oCr" #'org-resolve-clocks

            "of" '(:ignore t :wk "feeds")
            "ofi" #'org-feed-goto-inbox
            "ofu" #'org-feed-update-all))
  :config (progn
            ;; (modify-syntax-entry ?~ "$~" org-mode-syntax-table)
            ;; (modify-syntax-entry ?= "$=" org-mode-syntax-table)
            ;; (modify-syntax-entry ?/ "$/" org-mode-syntax-table)
            ;; (modify-syntax-entry ?* "$*" org-mode-syntax-table)
            ;; (modify-syntax-entry ?_ "$_" org-mode-syntax-table)
            (compdef
             :modes 'org-mode
             :capf '(pcomplete-completions-at-point
                     dabbrev-completion
                     helm-insert-file-name-completion-at-point
                     t))
            (general-def
              :keymaps 'org-mode-map
              "RET" #'org-open-at-point)
            (general-def
              :keymaps 'org-mode-map
              :states my:core:holy-states
              ;; rebind since pcomplete is now under TAB
              "<M-tab>" #'helm-complete-file-name-at-point)
            (hercules-def
             :toggle-funs #'my:org::babel-hercules
             :keymap 'org-babel-map
             :transient t)
            (my:org::general-def
              "a" #'org-agenda
              "p" #'org-priority
              "L" #'org-shiftright
              "H" #'org-shiftleft
              "J" #'org-shiftdown
              "K" #'org-shiftup
              "C-S-l" #'org-shiftcontrolright
              "C-S-h" #'org-shiftcontrolleft
              "C-S-j" #'org-shiftcontroldown
              "C-S-k" #'org-shiftcontrolup
              "," #'org-ctrl-c-ctrl-c
              "'" #'org-edit-special
              "*" #'org-ctrl-c-star
              "-" #'org-ctrl-c-minus
              "#" #'org-update-statistics-cookies
              "RET" #'org-ctrl-c-ret
              "M-RET" #'org-meta-return
              "A" #'org-attach

              "C" '(:ignore t :wk "clock")
              "Cc" #'org-clock-cancel
              "Cd" #'org-clock-display
              "Ce" #'org-evaluate-time-range
              "Cg" #'org-clock-goto
              "Ci" #'org-clock-in
              "CI" #'org-clock-in-last
              "Co" #'org-clock-out
              "CR" #'org-clock-report
              "Cr" #'org-resolve-clocks

              "d" '(:ignore t :wk "date")
              "dd" #'org-deadline
              "ds" #'org-schedule
              "dt" #'org-time-stamp
              "dT" #'org-time-stamp-inactive

              "e" #'org-export-dispatch

              "j" '(:ignore t :wk "jump")
              "jh" #'helm-org-in-buffer-headings
              "jp" #'helm-org-parent-headings
              "jl" #'helm-org-insert-link-to-heading-at-marker

              "f" '(:ignore t :wk "feed")
              "fi" #'org-feed-goto-inbox
              "fu" #'org-feed-update-all

              "T" '(:ignore t :wk "toggle")
              "Tc" #'org-toggle-checkbox
              "Te" #'org-toggle-pretty-entities
              "Ti" #'org-toggle-inline-images
              "Tl" #'org-toggle-link-display
              "Tt" #'org-show-todo-tree
              "TT" #'org-todo
              "TV" #'space-doc-mode
              "Tx" #'org-latex-preview

              "s" '(:ignore t :wk "subtree")
              "sa" #'org-toggle-archive-tag
              "sA" #'org-archive-subtree
              "sb" #'org-tree-to-indirect-buffer
              "sd" #'org-cut-subtree
              "sh" #'org-promote-subtree
              "sj" #'org-move-subtree-down
              "sk" #'org-move-subtree-up
              "sl" #'org-demote-subtree
              "sn" #'org-narrow-to-subtree
              "sN" #'widen
              "sr" #'org-refile
              "ss" #'org-sparse-tree
              "sS" #'org-sort

              "t" '(:ignore t :wk "table")
              "ta" #'org-table-align
              "tb" #'org-table-blank-field
              "tc" #'org-table-convert

              "td" '(:ignore t :wk "delete")
              "tdc" #'org-table-delete-column
              "tdr" #'org-table-kill-row
              "te" #'org-table-eval-formula
              "tE" #'org-table-export
              "th" #'org-table-previous-field
              "tH" #'org-table-move-column-left

              "ti" '(:ignore t :wk "insert")
              "tic" #'org-table-insert-column
              "tih" #'org-table-insert-hline
              "tiH" #'org-table-hline-and-move
              "tir" #'org-table-insert-row
              "tI" #'org-table-import
              "tj" #'org-table-next-row
              "tJ" #'org-table-move-row-down
              "tK" #'org-table-move-row-up
              "tl" #'org-table-next-field
              "tL" #'org-table-move-column-right
              "tn" #'org-table-create
              "tN" #'org-table-create-with-table.el
              "tr" #'org-table-recalculate
              "ts" #'org-table-sort-lines

              "tt" '(:ignore t :wk "toggle")
              "ttf" #'org-table-toggle-formula-debugger
              "tto" #'org-table-toggle-coordinate-overlays
              "tw" #'org-table-wrap-region

              "b" '(:ignore t :wk "babel")
              "b." #'my:org::babel-hercules
              "bp" #'org-babel-previous-src-block
              "bn" #'org-babel-next-src-block
              "be" #'org-babel-execute-maybe
              "bo" #'org-babel-open-src-block-result
              "bv" #'org-babel-expand-src-block
              "bu" #'org-babel-goto-src-block-head
              "bg" #'org-babel-goto-named-src-block
              "br" #'org-babel-goto-named-result
              "bb" #'org-babel-execute-buffer
              "bs" #'org-babel-execute-subtree
              "be" #'org-babel-execute-maybe
              "bd" #'org-babel-demarcate-block
              "bt" #'org-babel-tangle
              "bf" #'org-babel-tangle-file
              "bc" #'org-babel-check-src-block
              "bz" #'recenter-top-bottom
              "bj" #'org-babel-insert-header-arg
              "bl" #'org-babel-load-in-session
              "bi" #'org-babel-lob-ingest
              "bI" #'org-babel-view-src-block-info
              "bz" #'org-babel-switch-to-session
              "bZ" #'org-babel-switch-to-session-with-code
              "ba" #'org-babel-sha1-hash
              "bx" #'org-babel-do-key-sequence-in-edit-buffer

              "i" '(:ignore t :wk "insert")
              "ib" #'org-insert-structure-template
              "id" #'org-insert-drawer
              "ie" #'org-set-effort
              "if" #'org-footnote-new
              "ih" #'org-insert-heading
              "iH" #'org-insert-heading-after-current
              "ii" #'org-insert-item
              "il" #'org-insert-link
              "in" #'org-add-note
              "ip" #'org-set-property
              "is" #'org-insert-subheading
              "it" #'org-set-tags-command))
  
  :hook ((org-mode . auto-fill-mode)
         (org-mode . org-beamer-mode)
         (org-mode . yas-minor-mode)))
(use-package evil-org
  :after (org)
  ;; :hook wont work
  :init (progn
          (add-hook 'org-mode-hook #'evil-org-mode)
          (add-hook 'evil-org-mode-hook #'evil-org-set-key-theme))
  :config (progn
            (require 'evil-org-agenda)
            (evil-org-agenda-set-keys)))
(use-package ob
  :straight nil
  :after org
  :init (setq org-confirm-babel-evaluate nil)
  :config (org-babel-do-load-languages
           'org-babel-load-languages
           '((emacs-lisp . t)
             (python . t)
             (R . t))))
(use-package ob-async
  :after ob
  :init (setq ob-async-no-async-languages-alist '("ipython")))
(use-package org-bullets
  :after (org)
  :hook (org-mode . org-bullets-mode))

(use-package org-cliplink
  :after org
  :general (my:org::general-def
             "iL" #'org-cliplink))
(use-package org-gcal
  :commands (org-gcal-sync
             org-gcal-fetch
             org-gcal-post-at-point
             org-gcal-delete-at-point)
  :init (setq org-gcal-client-id
              (concat "800850979629-5jiat6cpagi2l7fgrlh6eucsb31ki8ch."
                      "apps.googleusercontent.com")
              org-gcal-client-secret "KvrrW1dBUF1U93ozXD9_Iz6W"
              org-gcal-file-alist
              '(("uros.m.perisic@gmail.com" .
                 "~/store/my_sync/org/gcal/uros.m.perisic.org")))
  :config (my:org::general-def
            "gp" #'org-gcal-post-at-point
            "gd" #'org-gcal-delete-at-point)
  :general (my:core::general-def
             "og" '(:ignore t :wk "gcal")
             "ogs" #'org-gcal-sync
             "ogf" #'org-gcal-fetch
             "ogr" #'org-gcal-refresh-token))
(use-package toc-org
  :after (org)
  :hook (org-mode . toc-org-mode))
(use-package orgit
  :defer t)

(provide 'my:org)

;; -*- lexical-binding: t; -*-
;; general

(use-package which-key
  :init
  (setq which-key-allow-evil-operators t
        which-key-allow-multiple-replacements t
        which-key-side-window-location 'right
        which-key-side-window-max-width 1.0
        which-key-max-display-columns 1
        which-key-show-prefix 'mode-line)
  :config (which-key-mode))
(use-package hercules
  :straight
  (hercules :type git
            :host gitlab
            :branch "develop"
            :repo "jjzmajic/hercules.el"
            :depth full)
  :config
  (hercules-def
   :show-funs #'which-key-show-top-level
   :hide-funs '(keyboard-quit keyboard-escape-quit)))
(use-package general
  :after which-key
  :init
  (progn
    (defun my:core::switch-to-scratch ()
      "Switch to the `*scratch*' buffer."
      (interactive)
      (let ((scratch-buffer "*scratch*"))
        (when (not (get-buffer scratch-buffer))
          (get-buffer-create scratch-buffer))
        (switch-to-buffer scratch-buffer)))

    (defun my:core::switch-to-messages ()
      "Switch to the `*Messages*' buffer."
      (interactive)
      (switch-to-buffer (messages-buffer)))

    (defun my:core::copy-path ()
      (interactive)
      (let ((file-name (file-truename (buffer-file-name))))
      (kill-new file-name)
      (message file-name)))

    (defun my:core::open-file ()
      (interactive)
      (let ((process-connection-type nil))
        (start-process "" nil "xdg-open" (buffer-file-name))))

    (setq my:core:leader "SPC"
          my:core:localleader ","
          my:core:alt-leader "M-SPC"
          my:core:alt-localleader "M-SPC"
          my:core:states '(insert emacs hybrid iedit-insert
                                  normal motion visual lisp
                                  evilified replace iedit)))
  :config
  (progn
    (defun my:core::backward-kill-line (arg)
      (interactive "p")
      (kill-line (- 1 arg)))
    (general-def
      "C-u" #'my:core::backward-kill-line)
    (general-create-definer my:core::general-def
     :keymaps 'override
     :states my:core:states
     :prefix my:core:leader
     :non-normal-prefix my:core:alt-leader
     :prefix-command 'my:core::command
     :prefix-map 'my:core:map)
    (general-create-definer my:core::mmode-general-def
     :states my:core:states
     :prefix my:core:localleader
     :non-normal-prefix my:core:alt-localleader)
    (my:core::general-def
      "u" #'universal-argument

      "b" '(:ignore t :wk "buffer")
      "bs" #'my:core::switch-to-scratch
      "bm" #'my:core::switch-to-messages
      "bn" #'next-buffer
      "bp" #'previous-buffer
      "bd" #'kill-this-buffer
      "bb" #'helm-mini

      "j" '(:ignore t :wk "jump")
      "jf" #'find-function
      "jv" #'find-variable
      "jo" #'open-line

      "d" '(:ignore t :wk "debug")
      "dd" #'debug-on-entry
      "dv" #'debug-on-variable-change

      "dc" '(:ignore t :wk "cancel")
      "dcd" #'cancel-debug-on-entry
      "dcv" #'cancel-debug-on-variable-change

      "w" '(:ignote t :wk "window")
      "wf" #'maximize-window ; focus
      "wm" #'delete-other-windows ; main
      "wd" #'delete-window
      "w-" #'split-window-below
      "w/" #'split-window-right

      "ws" '(:ignore t :wk "split")
      "wsh" #'split-window-below ; horizontal
      "wsv" #'split-window-right ; vertical

      "q" '(:ignore t :wk "quit")
      "qq" #'kill-emacs

      "F" '(:ignore t :wk "frame")
      "Fd" #'delete-frame

      "f" '(:ignore t :wk "file")
      "fo" #'my:core::open-file
      "fc" #'copy-file
      "fs" #'save-buffer
      "fS" #'evil-write-all
      "fR" #'vc-rename-file

      "x" '(:ignore t :wk "text")
      "x TAB" #'indent-rigidly
      "x SPC" #'cycle-spacing
      "xf" (general-simulate-key "M-q")
      "xS" #'sort-lines
      "xd" #'delete-trailing-whitespace

      "xw" '(:ignore t :wk "word")
      "xwc" #'count-words
      "xwr" #'count-words-region

      "xj" '(:ignore t :wk "justify")
      "xjl" #'set-justification-left
      "xjc" #'set-justification-center
      "xjr" #'set-justification-right
      "xjf" #'set-justification-full
      "xjn" #'set-justification-none

      "t" '(:ignore t :wk "toggle")
      "td" #'toggle-debug-on-error
      "th" #'hs-hide-all
      "ts" #'hs-show-all

      "f" '(:ignore t :wk "file")
      "fy" #'my:core::copy-path

      "a" '(:ignore t :wk "app")
      "au" #'undo-tree-visualize
      "ac" #'calc
      "ad" #'dired
      "ai" #'ielm

      "k" '(:ignore t :wk "key")
      "kg" #'general-describe-keybindings
      "kw" #'which-key-show-top-level
      ;; +++
      )))

;; evil
(use-package evil
  :gfhook
  ('(prog-mode-hook text-mode-hook)
   #'hs-minor-mode) ; make z bindings work
  :init
  (progn
    (setq evil-disable-insert-state-bindings t
          evil-want-keybinding nil
          evil-want-integration t
          evil-shift-width 2)
    (setq-default tab-width 4
                  indent-tabs-mode nil
                  tab-always-indent 'complete)
    (defun my:core::evil-shift-right ()
      (interactive)
      (evil-shift-right (region-beginning) (region-end))
      (evil-normal-state)
      (evil-visual-restore))
    (defun my:core::evil-shift-left ()
      (interactive)
      (evil-shift-left (region-beginning) (region-end))
      (evil-normal-state)
      (evil-visual-restore))
    (general-def
     :states 'visual
     ">" #'my:core::evil-shift-right
     "<" #'my:core::evil-shift-left))
  :general
  (my:core::general-def
    "j" '(:ignore t :wk "jump")
    "jb" #'evil-jump-backward
    "jf" #'evil-jump-forward)
  :config
  (progn
    (evil-mode +1)
    (general-def
      [escape] #'keyboard-escape-quit)))
(use-package evil-lisp-state
  :after evil
  :defer t
  :commands
  (evil-lisp-state
   lisp-state-eval-sexp-end-of-line)
  :general
  (my:core::general-def
    "l" #'lisp-state-toggle-lisp-state))
(use-package evil-iedit-state
  :after evil
  :general
  (my:core::general-def
    "s" '(:ignore t :wk "seach")
    "se" #'evil-iedit-state/iedit-mode))
(use-package targets
  :after (evil)
  :straight
  (targets
   :type git
   :host github
   :repo "noctuid/targets.el")
  :after (evil)
  :config (targets-setup t))
(use-package evil-textobj-line)
(use-package evil-textobj-anyblock)
(use-package evil-textobj-entire
  :after evil
  :general
  (general-def
    :keymaps
    '(evil-outer-text-objects-map
      evil-inner-text-objects-map)
    "e" #'evil-entire-entire-buffer))
(use-package evil-indent-plus
  :config (evil-indent-plus-default-bindings))
(use-package evil-lion
  :config (evil-lion-mode))
(use-package evil-surround
  :after (evil)
  :config (global-evil-surround-mode +1)
  :init
  (general-def
    :states 'visual
    :keymaps 'evil-surround-mode-map
    "s" #'evil-surround-region
    "S" #'evil-substitute))
(use-package evil-nerd-commenter
  :commands evilnc-comment-operator
  :defer t
  :after evil
  :init (setq evilnc-invert-comment-line-by-line nil)
  :config
  (my:core::general-def
    "c" '(:ignore t :wk "comments")
    "cl" #'evilnc-comment-or-uncomment-lines
    "cp" #'evilnc-comment-or-uncomment-paragraphs
    "ct" #'evilnc-comment-or-uncomment-to-the-line
    "cy" #'evilnv-copy-and-comment-lines)
  :general
  (general-def
    :keymaps 'evil-normal-state-map
    "gc" #'evilnc-comment-operator
    "gy" #'my:core:copy-and-comment-lines
    ";"  #'evilnc-comment-operator))
(use-package evil-collection
  :after (evil)
  :config (evil-collection-init))
(use-package expand-region
  :general
  (my:core::general-def
    "v" 'er/expand-region)
  :config
  (setq expand-region-contract-fast-key "V"
        expand-region-reset-fast-key "r"))

;; helm
(use-package helm
  :defer nil
  :init
  (progn
    (defun my:core::helm-faces ()
      "Describe face with `helm'."
      (interactive)
        (require 'helm-elisp)
        (let ((default (or (face-at-point) (thing-at-point 'symbol))))
          (helm :sources (helm-def-source--emacs-faces
                          (format "%s" (or default "default")))
                :buffer "*helm faces*")))

      (setq helm-display-buffer-default-height 0.4
            helm-default-display-buffer-functions
            '(display-buffer-in-side-window)
            helm-show-completion-display-function
            #'helm-default-display-buffer
            helm-split-window-inside-p t
            help-window-select t)
      (advice-add #'helm-occur :before
                  #'hs-show-all)
      (general-def
        "M-x" #'helm-M-x)
      (general-def
       :keymaps 'helm-map
       :package 'helm
       "TAB" #'helm-execute-persistent-action
       [backtab] #'helm-select-action))
  :config (helm-mode +1)
  :general
  (my:core::general-def
    "SPC" #'helm-M-x
    "h" '(:ignore t :wk "helm")
    "s" '(:ignore t :wk "search")
    "ss" #'helm-occur
    "sr" #'helm-resume

    "r" '(:ignore t :wk "ring")
    "rk" #'helm-show-kill-ring
    "rg" #'helm-global-mark-ring
    "rm" #'helm-mark-ring
    "rM" #'helm-all-mark-rings

    "h" '(:ignore t :wk "help")
    "ha" #'helm-apropos
    ;; help woman!
    "hw" #'helm-man-woman
    "hF" #'my:core::helm-faces

    "f" '(:ignore t :wk "file")
    "ff" #'helm-find-files
    "fF" #'helm-find
    "fl" #'helm-locate
    "fr" #'helm-recentf

    "x" '(:ignore t :wk "text")
    "xk" #'helm-show-kill-ring

    "j" '(:ignore t :wk "jump")
    "ji" #'helm-imenu
    "jI" #'helm-imenu-in-all-buffers
    "jr" #'helm-all-mark-rings

    "o" '(:ignore t :wk "org")
    "oj" #'helm-org-agenda-files-headings ; jump
    ;; +++
    ))
(use-package helm-ag
  :init (setq helm-ag-insert-at-point 'symbol)
  :ensure-system-package (ag . the_silver_searcher)
  :config
  (progn
    (general-def
      :keymaps 'helm-ag-map
      my:core:alt-leader #'helm-ag-edit)
      (my:core::mmode-general-def
        :keymaps 'helm-ag-edit-map
        "," #'helm-ag--edit-commit
        "c" #'helm-ag--edit-commit
        "k" #'helm-ag--edit-abort))
  :general
  (my:core::general-def
    "s" '(:ignore t :wk "search")
    "sd" #'helm-do-ag
    "sf" #'helm-do-ag-this-file
    "sp" #'helm-do-ag-project-root
    "sb" #'helm-do-ag-buffers))
(use-package helm-descbinds
  :general
  (my:core::general-def
    "k" '(:ignore t :wk "key")
    "kh" #'helm-descbinds
    ;; C-u
    "ku" #'describe-prefix-bindings
    "kb" #'describe-buffer-bindings
    "km" #'describe-mode-local-bindings
    "kp" #'describe-personal-keybindings)
  :config (helm-descbinds-mode))
(use-package wgrep-helm
  :disabled t
  :defer t
  :init (setq wgrep-auto-save-buffer t)
  :gfhook ('wgrep-setup-hook #'wgrep-change-to-wgrep-mode)
  :config
  (my:core::mmode-general-def
    :keymaps 'wgrep-mode-map
    "," #'wgrep-finish-edit
    "c" #'wgrep-finish-edit
    "k" #'wgrep-abort-changes
    "u" #'wgrep-remove-change
    "U" #'wgrep-remove-all-change
    "r" #'wgrep-toggle-readonly-area)
  :general
  (general-def
    :keymaps 'helm-map
    my:core:alt-leader #'helm-occur-run-save-buffer))

;; frameworks
(use-package spacemacs
  :no-require t
  :straight
  (spacemacs
   :type git
   :host github
   :repo "syl20bnr/spacemacs"
   :branch "develop"))
(use-package doom-emacs
  :no-require t
  :straight
  (doom-emacs
   :type git
   :host github
   :repo "hlissner/doom-emacs"
   :branch "develop"))

(provide 'my:core)

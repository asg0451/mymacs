;; -*- lexical-binding: t; -*-
(require 'my:core)
(require 'my:code)

(use-package markdown-mode
    :defer t
    :init
    (general-create-definer my:markdown::general-def
      :wrapping my:core::mmode-general-def
      :prefix-command #'my:markdown::command
      :prefix-map 'my:markdown:map
      :keymaps 'markdown-mode-map)
    :config
    (progn
      (my:markdown::general-def
        "{"   #'markdown-backward-paragraph
        "}"   #'markdown-forward-paragraph
        "]"   #'markdown-complete
        ">"   #'markdown-indent-region
        "<"   #'markdown-outdent-region
        "k"   #'markdown-kill-thing-at-point
        "N"   #'markdown-next-link
        "f"   #'markdown-follow-thing-at-point
        "P"   #'markdown-previous-link
        "M-RET" #'markdown-insert-list-item
        "RET" #'markdown-do
        [return] #'markdown-do

        "c" '(:ignore t :wk "command")
        "c]"  #'markdown-complete-buffer
        "cc"  #'markdown-check-refs
        "ce"  #'markdown-export
        "cm"  #'markdown-other-window
        "cn"  #'markdown-cleanup-list-numbers
        "co"  #'markdown-open
        "cp"  #'markdown-preview
        "cv"  #'markdown-export-and-preview
        "cw"  #'markdown-kill-ring-save
        "cP" #'markdown-live-preview-mode

        "h" '(:ignore t :wk "header")
        "hi"  #'markdown-insert-header-dwim
        "hI"  #'markdown-insert-header-setext-dwim
        "h1"  #'markdown-insert-header-atx-1
        "h2"  #'markdown-insert-header-atx-2
        "h3"  #'markdown-insert-header-atx-3
        "h4"  #'markdown-insert-header-atx-4
        "h5"  #'markdown-insert-header-atx-5
        "h6"  #'markdown-insert-header-atx-6
        "h!"  #'markdown-insert-header-setext-1
        "h@"  #'markdown-insert-header-setext-2

        "i" '(:ignore t :wk "insert")
        "ih"   #'markdown-insert-hr
        "if"  #'markdown-insert-footnote
        "ii"  #'markdown-insert-image
        "il"  #'markdown-insert-link
        "iw"  #'markdown-insert-wiki-link
        "iu"  #'markdown-insert-uri
        "il"  #'markdown-insert-list-item

        "T" '(:ignore t :wk "toggle")
        "Ti"  #'markdown-toggle-inline-images
        "Tl"  #'markdown-toggle-url-hiding
        "Tm"  #'markdown-toggle-markup-hiding
        "Tt"  #'markdown-toggle-gfm-checkbox
        "Tw"  #'markdown-toggle-wiki-links

        "t" '(:ignore t :wk "table")
        "tp"  #'markdown-table-move-row-up
        "tn"  #'markdown-table-move-row-down
        "tf"  #'markdown-table-move-column-right
        "tb"  #'markdown-table-move-column-left
        "tr"  #'markdown-table-insert-row
        "tR"  #'markdown-table-delete-row
        "tc"  #'markdown-table-insert-column
        "tC"  #'markdown-table-delete-column
        "ts"  #'markdown-table-sort-lines
        "td"  #'markdown-table-convert-region
        "tt"  #'markdown-table-transpose

        "x" '(:ignore t :wk "text")
        "xb"  #'markdown-insert-bold
        "xB"  #'markdown-insert-gfm-checkbox
        "xc"  #'markdown-insert-code
        "xC"  #'markdown-insert-gfm-code-block
        "xi"  #'markdown-insert-italic
        "xp"  #'markdown-insert-pre
        "xq"  #'markdown-insert-blockquote
        "xs"  #'markdown-insert-strike-through
        "xQ"  #'markdown-blockquote-region
        "xP"  #'markdown-pre-region)
      (general-def
        :keymaps 'markdown-mode-map
        ;; "gj" #'outline-forward-same-level
        ;; "gk" #'outline-backward-same-level
        ;; "gh" #'outline-up-heading
        ;; "gl" #'outline-next-visible-heading
        "M-<down>" #'markdown-move-down
        "M-<left>" #'markdown-move-promote
        "M-<right>" #'markdown-move-demote
        "M-<up>" #'markdown-move-up)))
(use-package gh-md
  :after markdown-mode
  :general
  (my:markdown::general-def
    "cr" 'gh-md-render-buffer))

(provide 'my:markdown)

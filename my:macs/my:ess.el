;; -*- lexical-binding: t; -*-
(require 'my:core)
(require 'my:code)

(use-package ess
  :defer t
  :gfhook ('ess-mode-hook
           #'electric-pair-mode)
  :preface
  (general-create-definer my:ess::general-def
    :wrapping my:core::mmode-general-def
    :keymaps 'ess-mode-map
    :major-modes t)
  :init
  (progn
    (general-add-hook 'ess-help-mode-hook #'evil-motion-state)
    (defun my:ess::indent-buffer ()
      (interactive)
      (save-excursion
        (indent-region (point-min) (point-max))))
    (setq ess-offset-continued 2
          ess-use-ido nil
          ess-use-flymake nil
          ess-offset-continued 0
          ess-expression-offset 2
          ess-nuke-trailing-whitespace-p t
          ess-style 'DEFAULT))
  :general
  (my:core::mmode-general-def
    :keymaps 'inferior-ess-mode-map
    ","  #'ess-smart-comma
    "ss" #'ess-switch-to-inferior-or-script-buffer
    "h" #'ess-doc-map
    "r" #'ess-extra-map
    "w" #'ess-r-package-dev-map
    "d" #'ess-dev-map)
  :config
  (progn
    (compdef
     :modes 'ess-mode
     :capf '(ess-filename-completion
             ess-r-object-completion
             dabbrev-completion
             ;; helm-yas-complete
             t))
    (with-eval-after-load 'ess-r-mode
      (modify-syntax-entry
       ?% "$%" inferior-ess-r-mode-syntax-table))
    (general-def
      :keymaps 'ess-doc-map
      "h" #'ess-display-help-on-object)
    (general-def :keymaps 'ess-mode-map
      [M-return] #'ess-eval-line-and-step)
    (general-def :keymaps 'inferior-ess-r-mode-map
      "M-n" #'comint-next-input
      "M-p" #'comint-previous-input)
    (my:ess::general-def
      ","  #'ess-eval-region-or-function-or-paragraph-and-step
      "=" #'my:ess::indent-buffer
      "h" #'ess-doc-map
      "r" #'ess-extra-map
      "w" #'ess-r-package-dev-map
      "d" #'ess-dev-map

      "s" '(:ignore t :wk "shell")
      "ss" #'ess-switch-to-inferior-or-script-buffer
      "sS" #'ess-switch-process
      "sB" #'ess-eval-buffer-and-go
      "sb" #'ess-eval-buffer
      "sd" #'ess-eval-region-or-line-and-step
      "sD" #'ess-eval-function-or-paragraph-and-step
      "sL" #'ess-eval-line-and-go
      "sl" #'ess-eval-line
      "sR" #'ess-eval-region-and-go
      "sr" #'ess-eval-region
      "sF" #'ess-eval-function-and-go
      "sf" #'ess-eval-function

      "c" '(:ignore t :wk "chunk")
      "cC" #'ess-eval-chunk-and-go
      "cc" #'ess-eval-chunk
      "cd" #'ess-eval-chunk-and-step
      "cm" #'ess-noweb-mark-chunk
      "cN" #'ess-noweb-previous-chunk
      "cn" #'ess-noweb-next-chunk)))
(use-package ess-view
  :after ess
  :general
  (my:core::mmode-general-def
    :keymaps 'inferior-ess-mode-map
    "o" '(:ignore t :wk "open")
    "ow" #'ess-view-inspect-df ; view
    "oe" #'ess-view-inspect-and-save-df ; edit
    ))

(use-package helm-R
  :after ess)
(use-package ess-R-data-view
  :after ess
  :init
  (general-def
    :keymaps 'ess-doc-map
    "p" #'ess-R-dv-pprint
    "t" #'ess-R-dv-ctable))

(provide 'my:ess)
